package com.ffspace.demo;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ConsumerController {
	/*
	 * @Autowired RestTemplate restTemplate;
	 * 
	 * @RequestMapping(value = "/add", method = RequestMethod.GET) public String
	 * add() { return restTemplate.getForEntity(
	 * "http://COMPUTE-SERVICE/add?a=10&b=20", String.class).getBody(); }
	 */

	@Autowired
	ComputeClient computeClient;

	@ApiOperation(value = "demo", notes = "test add")
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public Integer add() {
		return computeClient.add(30, 20);
	}
}
