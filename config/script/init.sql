create table t_user
(
   login_name           varchar(20) not null,
   password             varchar(20) not null,
   nick_name            varchar(20) not null,
   email                varchar(100) not null,
   id                   INT(20) not null AUTO_INCREMENT,
   status               int(2) not null default 0,
   create_time 			datetime not null,
   update_time 			datetime not null,
   primary key (id)
);



create table t_blog
(
  id                   INT(20) not null AUTO_INCREMENT,
  user_id              INT(20) not null,
  title				   varchar(200) not null,
  content              LONGTEXT not null,
  status               int(2) not null default 1,
  create_time 			datetime not null,
  update_time 			datetime not null,
  visible              int(2) default 0,
  primary key (id)
);

create table t_comment
(
  id                   INT(20) not null AUTO_INCREMENT,
  blog_id			   INT(20) not null,
  comment              varchar(400) not null,
  status               int(2) not null default 1,
  create_time 			datetime not null,
  update_time 			datetime not null,  
  user_id				INT(20),
  display_name			varchar(100),
  primary key (id)
);





CREATE UNIQUE INDEX index_user_login_name ON t_user(login_name(20));
CREATE UNIQUE INDEX index_user_nick_name ON t_user(nick_name(20));
CREATE UNIQUE INDEX index_user_email ON t_user(email(100));