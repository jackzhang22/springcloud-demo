package com.ffspace.blog.service;

import java.util.List;
import java.util.Map;

import com.ff.blog.common.BaseResult;
import com.ffspace.blog.core.dal.dataobject.Blog;

public interface BlogService {
	
	BaseResult<Blog> createBlog(Blog blog);
	
	BaseResult<String> deleteBlog(Integer id);
	
	BaseResult<Blog> updateBlog(Blog blog);
	
	BaseResult<List<Blog>> queryPage(Map<String, Object> condition);

}
