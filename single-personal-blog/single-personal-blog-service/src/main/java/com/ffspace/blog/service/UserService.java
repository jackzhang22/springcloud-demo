package com.ffspace.blog.service;

import com.ff.blog.common.BaseResult;
import com.ffspace.blog.core.dal.dataobject.User;

public interface UserService {

	void register(User user);
	
	BaseResult<User> login(String loginName, String password);
	
	BaseResult<String> active(String token);
	
}
