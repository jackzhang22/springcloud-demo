package com.ffspace.blog.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ff.blog.common.BaseResult;
import com.ff.blog.enums.BlogStatus;
import com.ffspace.blog.core.dal.dataobject.Blog;
import com.ffspace.blog.core.dal.mapper.BlogMapper;

@Component
public class BlogServiceImpl implements BlogService {

	private static Logger	logger = LoggerFactory.getLogger(BlogServiceImpl.class);
	
	@Autowired
	private BlogMapper blogMapper;

	public BaseResult<Blog> createBlog(Blog blog) 
	{
		
		BaseResult<Blog> result = new BaseResult<Blog>();
		
		try 
		{
			if (blog.getVisible() == null)
			{
				blog.setVisible(0);
			}
			blog.setStatus(BlogStatus.NORMAL.getCode());
			Date now = new Date();
			blog.setCreateTime(now);
			blog.setUpdateTime(now);
			blogMapper.insert(blog);
			result.setSuccess(true);
			result.setMessage("创建成功");
		}
		catch (Exception e)
		{
			logger.error("createBlog error", e);
			result.setSuccess(false);
			result.setMessage("服务器错误");
		}
		
		return result;
	}

	public BaseResult<String> deleteBlog(Integer id) {
		BaseResult<String> result = new BaseResult<String>();
		try
		{
			Blog blog = new Blog();
			blog.setId(id);
			blog.setStatus(BlogStatus.DELETED.getCode());
			blogMapper.updateByPrimaryKeySelective(blog);
			result.setSuccess(true);
			result.setMessage("删除成功");
		}
		catch (Exception e)
		{
			logger.error("deleteBlog error", e);
			result.setSuccess(false);
			result.setMessage("服务器错误");
		}
		return result;
	}

	public BaseResult<Blog> updateBlog(Blog blog) {
		BaseResult<Blog> result = new BaseResult<Blog>();
		try
		{
			blogMapper.updateByPrimaryKeySelective(blog);
			result.setSuccess(true);
			result.setMessage("更新成功");
		}
		catch (Exception e)
		{
			logger.error("updateBlog error", e);
			result.setSuccess(false);
			result.setMessage("服务器错误");
		}
		return result;
	}

	public BaseResult<List<Blog>> queryPage(Map<String, Object> condition) {
		BaseResult<List<Blog>> result = new BaseResult<List<Blog>>();
		try
		{
			List<Blog> blogList = blogMapper.selectByConditionPage(condition);
			result.setData(blogList);
			result.setSuccess(true);
			result.setMessage("查询成功");
		}
		catch (Exception e)
		{
			logger.error("queryPage error", e);
			result.setSuccess(false);
			result.setMessage("服务器错误");
		}
		return result;
	}

}
