package com.ff.blog.common;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.auth0.jwt.Algorithm;
import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;


public class CommonTool {
	
	private static Logger	logger = LoggerFactory.getLogger(CommonTool.class);
	
	public static String MD5(String plainText) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte b[] = md.digest();

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			// 32位加密
			return buf.toString();
			// 16位的加密
			// return buf.toString().substring(8, 24);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public static String generateToken(Integer userId, PrivateKey signKey)
	{
        final HashMap<String, Object> claims = new HashMap<String, Object>();
        claims.put("mid", userId);

        long exp = (new Date().getTime()+ 24*60*60*1000)/1000;
        claims.put("exp", exp);
        
        final JWTSigner jwtSigner = new JWTSigner(signKey);
        final JWTSigner.Options options = new JWTSigner.Options();
        options.setAlgorithm(Algorithm.RS256);
        final String tokenString = jwtSigner.sign(claims, options);
        return tokenString;
	}
	
	public static Map<String, Object> parseToken(String token, PublicKey pubKey)
	{
		Map<String, Object> claims = null;
    	try {
    		final JWTVerifier verifier = new JWTVerifier(pubKey);
    	    claims= verifier.verify(token);
    	} catch (JWTVerifyException e) {
    		logger.error("parseToken error: ", e);
			
    	} catch (InvalidKeyException e) {
    		logger.error("parseToken error: ", e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("parseToken error: ", e);
		} catch (IllegalStateException e) {
			logger.error("parseToken error: ", e);
		} catch (SignatureException e) {// 签名问题;
			logger.error("parseToken error: ", e);
		} catch (IOException e) {
			logger.error("parseToken error: ", e);
		} catch (Exception e) {
			logger.error("parseToken error: ", e);
		}
    	return claims;
	}
}
