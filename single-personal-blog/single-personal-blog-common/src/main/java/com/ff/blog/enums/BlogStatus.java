package com.ff.blog.enums;

public enum BlogStatus {
	DELETED(0, "已删除"),
	NORMAL(1, "正常");
	
	private final int code;
	private final String value;
	
	private BlogStatus(int code, String value)
	{
		this.code = code;
		this.value = value;
	}

	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}	
	
}
