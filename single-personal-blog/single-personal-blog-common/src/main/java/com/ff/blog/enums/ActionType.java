package com.ff.blog.enums;

public enum ActionType {
	
	ACTIVE("active", "激活账号"),
	REGISTE("register", "注册账号"),
	LOGIN("login", "登录成功");
	
	private final String code;
	private final String value;
	private ActionType(String code, String value)
	{
		this.code = code;
		this.value = value;
	}
	public String getCode() {
		return code;
	}
	public String getValue() {
		return value;
	}
	
	
}
