package com.ff.blog.enums;

public enum ErrorCode {
	NOT_LOGIN(9, "未登录"); 

	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	private final int code;
	private final String value;
	
	private ErrorCode(int code, String value)
	{
		this.code = code;
		this.value = value;
	}
}
