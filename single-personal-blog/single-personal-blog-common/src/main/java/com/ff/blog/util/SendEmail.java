package com.ff.blog.util;

// 需要用户名密码邮件发送实例
//本实例以QQ邮箱为例，你需要在qq后台设置
 
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.auth0.jwt.internal.org.apache.commons.lang3.StringUtils;
 
public class SendEmail
{

   public static void send(String to, final String from, String host, final String emailPwd, String subject, String text, String fileName, String filePath){
	      // 获取系统属性
	      Properties properties = System.getProperties();
	      // 设置邮件服务器
	      properties.setProperty("mail.smtp.host", host);
	      properties.put("mail.smtp.port", "25");
	      properties.put("mail.smtp.auth", "true");
	      // 获取默认session对象
	      Session session = Session.getDefaultInstance(properties,
					new Authenticator() {
						public PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(from,
									emailPwd); // 发件人邮件用户名、密码
						}
					});
	 
	      try{
	         // 创建默认的 MimeMessage 对象
	         MimeMessage message = new MimeMessage(session);
	         message.setFrom(new InternetAddress(from));
	         message.addRecipient(Message.RecipientType.TO,
	                                  new InternetAddress(to));
	         message.setSubject(subject);
	         
	         // 创建多重消息
	         Multipart multipart = new MimeMultipart();
	 
	         // 创建消息部分
	         BodyPart messageBodyPart = new MimeBodyPart();
	         messageBodyPart.setText(text);
	         multipart.addBodyPart(messageBodyPart);
	 
	         // 附件部分
	         if (StringUtils.isNotBlank(fileName) 
	        		 && StringUtils.isNotBlank(filePath))
	         {
		         messageBodyPart = new MimeBodyPart();
		         DataSource source = new FileDataSource(filePath);
		         messageBodyPart.setDataHandler(new DataHandler(source));
		         messageBodyPart.setFileName(fileName);
		         multipart.addBodyPart(messageBodyPart);
	         }
	 
	         // 发送完整消息
	         message.setContent(multipart);
	         
	         // 发送消息
	         Transport.send(message);
	         System.out.println("Sent mail successfully");
	      }catch (MessagingException mex) {
	         mex.printStackTrace();
	      }
   }
   
   public static void main(String [] args)
   {
      // 收件人电子邮箱
      String to = "xxx@qq.com";
 
      // 发件人电子邮箱
      String from = "xxx@qq.com";
 
      // 指定发送邮件的主机为 smtp.qq.com
      String host = "smtp.qq.com";  //QQ 邮件服务器
 
      // 获取系统属性
      Properties properties = System.getProperties();
 
      // 设置邮件服务器
      properties.setProperty("mail.smtp.host", host);
 
      properties.put("mail.smtp.auth", "true");
      // 获取默认session对象
      Session session = Session.getDefaultInstance(properties,
				new Authenticator() {
					public PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication("xxx@qq.com",
								"qq邮箱密码"); // 发件人邮件用户名、密码
					}
				});
 
      try{
         // 创建默认的 MimeMessage 对象
         MimeMessage message = new MimeMessage(session);
 
         // Set From: 头部头字段
         message.setFrom(new InternetAddress(from));
 
         // Set To: 头部头字段
         message.addRecipient(Message.RecipientType.TO,
                                  new InternetAddress(to));
 
         // Set Subject: 头部头字段
         message.setSubject("This is the Subject Line!");
 
        
         // 创建多重消息
         Multipart multipart = new MimeMultipart();
 
         // 创建消息部分
         BodyPart messageBodyPart = new MimeBodyPart();
         messageBodyPart.setText("This is message body");
         multipart.addBodyPart(messageBodyPart);
 
         
         // 附件部分
         messageBodyPart = new MimeBodyPart();
         String filename = "file.txt";
         DataSource source = new FileDataSource(filename);
         messageBodyPart.setDataHandler(new DataHandler(source));
         messageBodyPart.setFileName(filename);
         multipart.addBodyPart(messageBodyPart);
 
         // 发送完整消息
         message.setContent(multipart );
         
         // 发送消息
         Transport.send(message);
         System.out.println("Sent message successfully....from w3cschool.cc");
      }catch (MessagingException mex) {
         mex.printStackTrace();
      }
   }
   
}