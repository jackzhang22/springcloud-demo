package com.ff.blog.enums;

public enum UserStatus {
	
	UNACTIVE(0, "未激活"),
	ACTIVED(1, "已激活"),
	LOGOFF(2, "已注销");
	
	private final int code;
	private final String value;
	
	UserStatus(int code, String value)
	{
		this.code = code;
		this.value = value;
	}

	public int getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

}
