<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>个人博客</title>


<link rel="stylesheet" href="html/static/lib/css/bootstrap.min.css" />
<!-- <link rel="stylesheet"
	href="html/static/assets/css/ie10-viewport-bug-workaround.css" /> -->
<link rel="stylesheet" href="html/static/lib/css/bootstrap-select.css" />
<link rel="stylesheet" href="html/static/lib/css/bootstrap-switch.css" />
<link rel="stylesheet" href="html/static/lib/css/jquery.bootgrid.css" />
<link rel="stylesheet" href="html/static/lib/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="html/static/css/common.css" />

<script src="html/static/lib/js/jquery-2.0.0.min.js"></script>
<script src="html/static/lib/js/bootstrap.min.js"></script>

</head>
<body>







	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12"></div>
		</div>
		<div class="row-fluid">
			<div class="span3"
				style="width: 20%; float: left; border: 1px solid #FFFFFF;"></div>
			<div class="span6"
				style="width: 50%; border: 0px solid red; float: left; margin-top: 80px;">
				<div class="tabbable" id="tabs-193765">
					<c:if test="${type == 'register'}">

					欢迎您的注册！
					我们已经给您发了一封激活邮件，通过邮件中的链接激活您的帐户后，您就可以登录博客园。
					帐户激活后，您可以使用除博客之外的服务，开通博客需要单独申请。 如果未收到激活邮件，您可以重发激活邮件。
					如果重发激活邮件仍然没有收到，请检查一下是否在垃圾邮箱中。 注：遇到任何问题，请发邮件至 contact@cnblogs.com
					与我们联系。


					</c:if>

					<c:if test="${type == 'active'}">

						恭喜，你的账号已激活



					</c:if>


				</div>

			</div>
			<div class="span3"></div>
		</div>
		<div class="row-fluid">
			<div class="span12"></div>
		</div>
	</div>










	<nav id="J_Foot" class="navbar  navbar-fixed-bottom"></nav>



<script type="text/javascript">

$(document).ready(function(){
	
	$("#J_Foot").load("html/html/foot.html");
	
});

</script>




</body>
</html>