package com.ffspace.blog.core.dal.mapper;

import java.util.List;
import java.util.Map;

import com.ffspace.blog.core.dal.dataobject.Blog;

public interface BlogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Blog record);

    int insertSelective(Blog record);

    Blog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Blog record);

    int updateByPrimaryKeyWithBLOBs(Blog record);

    int updateByPrimaryKey(Blog record);
    
    List<Blog> selectByConditionPage(Map<String, Object> condition);
}